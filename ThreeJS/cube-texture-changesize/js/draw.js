var zoom_default = 5
//Create canvas, id = "thecanvas"
// var theCanvas = document.createElement("canvas")
// theCanvas.id = "theCanvas"
// document.getElementById("divcontainer").appendChild(theCanvas)
document.getElementById("divcontainer").innerHTML += "<canvas id='theCanvas' height='500' width='500'></canvas>"
var theCanvas = document.getElementById("theCanvas")
theCanvas.style.backgroundColor = "blue"
theCanvas.style.border = '1px solid #000'
theCanvas.style.borderRadius = "2px"
theCanvas.style.display = "block"
theCanvas.style.margin = '0 auto'

var scene = new THREE.Scene()
var camera = new THREE.PerspectiveCamera(45, theCanvas.width/theCanvas.height, 1, 1000)
	camera.position.set(-4, 2.5, 2)
var light = new THREE.DirectionalLight()
	light.position.set(0,0,1)
camera.add(light)
scene.add(camera)

var renderer = new THREE.WebGLRenderer({
	canvas: theCanvas,
	antialias: true
})

var controls = new THREE.OrbitControls(camera, theCanvas)
// disable zoom by scrol
controls.minDistance = zoom_default
controls.maxDixtance = zoom_default


//get image texture
var image = document.getElementById("imgtexture")	
var textures = THREE.ImageUtils.loadTexture(image.src, render);

var material_array = []
for (var i = 0; i < 6; i++) {
	material_array.push(new THREE.MeshPhongMaterial({
		color: "white",
		map: textures
	}))
}
render()
var material = new THREE.MeshFaceMaterial(material_array)

var geometry = new THREE.CubeGeometry(20,20,20)
var cube 		 = new THREE.Mesh(geometry, material)

// scale
scale()

scene.add(cube)

SetupDragAction(theCanvas, render)
render()

function render(){
	controls.update()
	renderer.render(scene, camera)
}

function SetupDragAction(element, DragAction){
	function move(){
		DragAction()
	}

	function down(){
		document.addEventListener("mousemove", move, false)
	}

	function up(){
		document.removeEventListener("mousemove", move, false)
	}
	element.addEventListener("mousedown", down, false)
}

window.onload = function(){
	render()
	$("#divcontainer").removeClass( "hide" )
}

function scale(x_scale, y_scale, z_scale){
	scale_default = 0.1
	x_scale = $.isNumeric(x_scale) ? x_scale : scale_default
	y_scale = $.isNumeric(y_scale) ? y_scale : scale_default
	z_scale = $.isNumeric(z_scale) ? z_scale : scale_default

	max = Math.max(x_scale, y_scale, z_scale)
	cube.scale.x = x_scale/max*scale_default
	cube.scale.y = y_scale/max*scale_default
	cube.scale.z = z_scale/max*scale_default
}