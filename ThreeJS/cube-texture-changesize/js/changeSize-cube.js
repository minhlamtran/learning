$(document).ready(function(){
	$("#confirm").click(function(){
		x_value = $("#x").val()
		y_value = $("#y").val()
		z_value = $("#z").val()
		scale(x_value, y_value, z_value)
		render()
	})

	$("#reset").click(function(){
		scale()
		camera.position.set(-4, 2.5, 2)
		render()
	})
})