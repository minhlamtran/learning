
document.getElementById("thecanvas").innerHTML += "<canvas id='cnvs' height='600' width='600'></canvas>"
thecanvas = document.getElementById("cnvs")
thecanvas.style.backgroundColor = "lightblue"

function init(){

	this.text = {}
	this.image = {}
	this.dynamicTexture = []
	this.face_current = 0

	
	scene = new THREE.Scene()
	camera = new THREE.PerspectiveCamera(45, thecanvas.width/thecanvas.height, 1, 1000)
	var light = new THREE.DirectionalLight()
		light.position.set(0, 0, 1)
	camera.add(light)
	this.change_camera_direction()
	scene.add(camera)

	renderer = new THREE.WebGLRenderer({
		canvas: thecanvas,
		antialias: true
	})

	controls = new THREE.OrbitControls(camera, thecanvas)
	// disable zoom by scroll
	controls.maxDistance = scrollDefault
	controls.minDistance = scrollDefault

	// create material for 6 face of cube
	var materials = []
	for (var i = 0; i < 6; i++) {

		this.dynamicTexture[i] = new THREEx.DynamicTexture(width, height)

		materials.push(new THREE.MeshPhongMaterial({
			color: "#fff",
			map: this.dynamicTexture[i].texture
		}))
		
		this.dynamicTexture[i].drawImage(image, 0, 0, 360, 360)
	}

	// create cube
	this.cube = new THREE.Mesh(new THREE.CubeGeometry(depth_default,height_default,width_default), new THREE.MeshFaceMaterial(materials))
	this.scale()
	scene.add(this.cube)
	// console.log(width)

	// this.dynamicTexture[0].drawText("Your text here!", undefined, 256, "#333", "normal 80px arial")

	this.SetupDragAction(thecanvas, this.render)
	this.render()
} // end init()


// render
init.prototype.render = function(){
	controls.update()
	renderer.render(scene, camera)
}

// Drag cube
init.prototype.SetupDragAction = function(element, DragAction){
	function move(){
		DragAction()
	}

	function down(){
		document.addEventListener("mousemove", move, false)
	}

	function up(){
		document.removeEventListener("mousemove", move, false)
	}

	element.addEventListener("mousedown", down, false)
}

// change camera direction
init.prototype.change_camera_direction = function(facenumber){
	facenumber = $.isNumeric(facenumber) ? facenumber : 0
	CP = CameraPositionArray[facenumber]
	camera.position.set(CP.x, CP.y, CP.z)
}

// scale cube
init.prototype.scale = function(x_scale, y_scale, z_scale){
	check = $.isNumeric(x_scale) && $.isNumeric(y_scale) && $.isNumeric(z_scale)
	if (!check) {
		this.cube.scale.x = this.cube.scale.y = this.cube.scale.z = scale_default
	}else{
		maxsize = Math.max(x_scale, y_scale, z_scale)
		this.cube.scale.x = x_scale/maxsize * scale_default
		this.cube.scale.y = y_scale/maxsize * scale_default
		this.cube.scale.z = z_scale/maxsize * scale_default
	}
	this.render()
}

// draw text
init.prototype.drawText = function(params){
	for (var i = 0; i < params.length; i++) {
		this.dynamicTexture[params[i].facenumber].drawText(params[i].text, params[i].x, params[i].y, params[i].color, params[i].font)
	}	
}

