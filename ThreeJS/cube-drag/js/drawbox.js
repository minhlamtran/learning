var thecanvas = document.getElementById("cnvs")
thecanvas.style.backgroundColor = "lightblue";

var scene = new THREE.Scene()
var camera = new THREE.PerspectiveCamera(45, thecanvas.width/thecanvas.height, 1, 10000)

var renderer = new THREE.WebGLRenderer({
	canvas: thecanvas,
	antialias: true
})

var light = new THREE.DirectionalLight()
light.position.set(0,0,1)
camera.add(light)
camera.position.z = 5

scene.add(camera)

var geometry = new THREE.CubeGeometry(20,20,20)
var material = new THREE.MeshBasicMaterial({color: 0x00ff00})
var cube = new THREE.Mesh(geometry, material)

scene.add(cube)

cube.scale.x = 0.1; // SCALE
cube.scale.y = 0.1; // SCALE
cube.scale.z = 0.1; // SCALE

var controls = new THREE.OrbitControls(camera, thecanvas)

SetupDragAction(thecanvas, render)
render()

function SetupDragAction(element, dragAction){
	function move(){
		dragAction()
	}

	function down(){
		document.addEventListener("mousemove", move, false)
	}

	function up(){
		document.removeEventListener("mousemove", move, false)
	}

	element.addEventListener("mousedown", down, false)
}

function render(){
	controls.update()
	renderer.render(scene, camera)
}